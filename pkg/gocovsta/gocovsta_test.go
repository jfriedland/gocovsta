/*
Package gocovsta - Go Coverage to Stash converts Cobertura XML coverage
reports into JSON output for use by the Stash code coverage plugin.
*/
package gocovsta

import (
	"encoding/xml"
	"os"
	"reflect"
	"testing"
)

var validCoverage = &Coverage{
	XMLName: xml.Name{
		Local: "coverage",
	},
	LineRate:     0.37062937,
	LinesCovered: 106,
	LinesValid:   286,
	Timestamp:    1554091579949,
	Packages: []Package{
		{
			Name:     "foo.example.com/mail/pkg/rfc822",
			LineRate: 0.37062937,
			Classes: []Class{
				{
					Name:     "decoder",
					Filename: "pkg/rfc822/rfc822.go",
					LineRate: 0.75,
					Methods: []Method{
						{
							Name:     "Decode",
							LineRate: 0.75,
							Lines: []Line{
								{
									Number: 359,
									Hits:   1,
								},
								{
									Number: 360,
									Hits:   1,
								},
								{
									Number: 361,
									Hits:   1,
								},
								{
									Number: 362,
									Hits:   1,
								},
								{
									Number: 363,
								},
								{
									Number: 364,
								},
								{
									Number: 366,
									Hits:   1,
								},
								{
									Number: 367,
									Hits:   1,
								},
							},
						},
					},
					Lines: []Line{
						{
							Number: 359,
							Hits:   1,
						},
						{
							Number: 360,
							Hits:   1,
						},
						{
							Number: 361,
							Hits:   1,
						},
						{
							Number: 362,
							Hits:   1,
						},
						{
							Number: 363,
						},
						{
							Number: 364,
						},
						{
							Number: 366,
							Hits:   1,
						},
						{
							Number: 367,
							Hits:   1,
						},
					},
					LineCount: 8,
					LineHits:  6,
				},
			},
			LineCount: 286,
			LineHits:  106,
		},
	},
	Sources: []string{
		"/home/user/go/src/foo.example.com/mail",
	},
}

var validFiles = []File{
	{
		Path:     "pkg/rfc822/rfc822.go",
		Coverage: "C:359,360,361,362,366,367;P:;U:363,364",
		hits: []int{
			359,
			360,
			361,
			362,
			366,
			367,
		},
		misses: []int{
			363,
			364,
		},
	},
}

var validOutput = Output{
	Files: validFiles,
}

func Test_intsToStrings(t *testing.T) {
	type args struct {
		ints []int
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			"Convert []int to []string, normal case",
			args{[]int{1, 2, 3, 4}},
			[]string{"1", "2", "3", "4"},
		},
		{
			"Convert []int to []string, empty case",
			args{[]int{}},
			[]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := intsToStrings(tt.args.ints); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("intsToStrings() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseInput(t *testing.T) {
	validFile, _ := os.Open("testdata/coverage-test-ok.xml")
	emptyFile, _ := os.Open("testdata/coverage-test-empty.xml")
	type args struct {
		f *os.File
	}
	tests := []struct {
		name    string
		args    args
		want    *Coverage
		wantErr bool
	}{
		{
			"Test XML parser, OK", args{f: validFile}, validCoverage, false,
		},
		{
			"Test XML parser, empty file", args{f: emptyFile}, nil, true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseInput(tt.args.f)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseInput() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseCoverage(t *testing.T) {
	type args struct {
		c *Coverage
	}
	tests := []struct {
		name string
		args args
		want *Output
	}{
		{"Test Coverage parse, OK", args{c: validCoverage}, &validOutput},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseCoverage(tt.args.c); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseCoverage() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOutput_JSON(t *testing.T) {
	type fields struct {
		Files []File
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{
			"Test JSON output, OK",
			fields{Files: validFiles},
			`{"files":[{"path":"pkg/rfc822/rfc822.go","coverage":"C:359,360,361,362,366,367;P:;U:363,364"}]}`,
			false,
		},
		{
			"Test JSON output, empty",
			fields{Files: nil},
			`{"files":null}`,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			o := &Output{
				Files: tt.fields.Files,
			}
			got, err := o.JSON()
			if (err != nil) != tt.wantErr {
				t.Errorf("Output.JSON() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Output.JSON() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOutput_JSON_nil(t *testing.T) {
	var o *Output
	got, err := o.JSON()
	if err == nil {
		t.Errorf("Output.JSON() error = %v, wantErr %v", err, true)
		return
	}
	if got != "" {
		t.Errorf("Output.JSON() = %v, want %v", got, "")
	}
}
