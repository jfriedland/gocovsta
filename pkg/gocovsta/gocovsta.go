/*
Package gocovsta implements the parsing and conversion logic for transforming
Cobertura XML coverage reports into JSON output, for consumption by the
Bitbucket Server code coverage plugin API.
*/
package gocovsta

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"strconv"
	"strings"
)

// Output JSON type.
type Output struct {
	Files []File `json:"files"`
}

// File JSON type.
type File struct {
	Path     string `json:"path"`
	Coverage string `json:"coverage"`
	// Line numbers, as hits/misses
	hits   []int
	misses []int
}

// intsToStrings converts a slice of ints to a slice of strings.
func intsToStrings(ints []int) []string {
	out := make([]string, len(ints))
	for i, value := range ints {
		out[i] = strconv.Itoa(value)
	}
	return out
}

// Hits returns hits as a comma-separated string.
func (f File) Hits() string {
	return strings.Join(intsToStrings(f.hits), ",")
}

// Misses returns misses as a comma-separated string.
func (f File) Misses() string {
	return strings.Join(intsToStrings(f.misses), ",")
}

// JSON returns the JSON representation of the coverage output.
func (o *Output) JSON() (string, error) {
	var err error
	if o == nil {
		return "", errors.New("output is nil")
	}
	r, err := json.Marshal(o)
	if err != nil {
		return "", err
	}
	return string(r), nil
}

// Coverage input data.
type Coverage struct {
	XMLName         xml.Name  `xml:"coverage"`
	LineRate        float32   `xml:"line-rate,attr"`
	BranchRate      float32   `xml:"branch-rate,attr"`
	LinesCovered    float32   `xml:"lines-covered,attr"`
	LinesValid      int64     `xml:"lines-valid,attr"`
	BranchesCovered int64     `xml:"branches-covered,attr"`
	BranchesValid   int64     `xml:"branches-valid,attr"`
	Complexity      float32   `xml:"complexity,attr"`
	Version         string    `xml:"version,attr"`
	Timestamp       int64     `xml:"timestamp,attr"`
	Packages        []Package `xml:"packages>package"`
	Sources         []string  `xml:"sources>source"`
}

// Package input data.
type Package struct {
	Name       string  `xml:"name,attr"`
	LineRate   float32 `xml:"line-rate,attr"`
	BranchRate float32 `xml:"branch-rate,attr"`
	Complexity float32 `xml:"complexity,attr"`
	Classes    []Class `xml:"classes>class"`
	LineCount  int64   `xml:"line-count,attr"`
	LineHits   int64   `xml:"line-hits,attr"`
}

// Class input data.
type Class struct {
	Name       string   `xml:"name,attr"`
	Filename   string   `xml:"filename,attr"`
	LineRate   float32  `xml:"line-rate,attr"`
	BranchRate float32  `xml:"branch-rate,attr"`
	Complexity float32  `xml:"complexity,attr"`
	Methods    []Method `xml:"methods>method"`
	Lines      []Line   `xml:"lines>line"`
	LineCount  int64    `xml:"line-count,attr"`
	LineHits   int64    `xml:"line-hits,attr"`
}

// Method input data.
type Method struct {
	Name       string  `xml:"name,attr"`
	Signature  string  `xml:"signature,attr"`
	LineRate   float32 `xml:"line-rate,attr"`
	BranchRate float32 `xml:"branch-rate,attr"`
	Complexity float32 `xml:"complexity,attr"`
	Lines      []Line  `xml:"lines>line"`
	LineCount  int64   `xml:"line-count,attr"`
	LineHits   int64   `xml:"line-hits,attr"`
}

// Line input data.
type Line struct {
	Number int   `xml:"number,attr"`
	Hits   int64 `xml:"hits,attr"`
}

// ParseInput unmarshals the input XML to a Coverage type.
func ParseInput(r io.Reader) (*Coverage, error) {
	var err error

	bytes, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	var coverage = new(Coverage)
	err = xml.Unmarshal(bytes, coverage)
	if err != nil {
		return nil, err
	}

	return coverage, nil
}

// ParseCoverage parses the input XML data and generates the Output for
// marshalling to JSON.
func ParseCoverage(c *Coverage) *Output {
	var output = new(Output)
	for _, pkg := range c.Packages {
		for _, cls := range pkg.Classes {
			var f File
			for _, line := range cls.Lines {
				if line.Hits == 0 {
					f.misses = append(f.misses, line.Number)
				}
				if line.Hits > 0 {
					f.hits = append(f.hits, line.Number)
				}
			}
			if len(f.hits) > 0 || len(f.misses) > 0 {
				f.Path = cls.Filename
				f.Coverage = fmt.Sprintf("C:%s;P:;U:%s", f.Hits(), f.Misses())
			}
			output.Files = append(output.Files, f)
		}
	}
	return output
}
