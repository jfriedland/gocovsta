# gocovsta

A CLI tool for transforming Cobertura XML coverage reports into JSON output, for
consumption by the Bitbucket Server code coverage plugin API

Maybe one day it will be able to parse native Go coverage reports directly, but
for now it relies on `gocov-xml` to do the coverage -> XML conversion.

## Install

    go get bitbucket.org/jfriedland/gocovsta

Other tools:

    go get -u -v github.com/axw/gocov
    go get -u -v github.com/AlekSi/gocov-xml

## Running

Generate coverage output as part of your CI pipeline:

```shell
# Go-specific testing; Cobertura/jcoverage will generate output/coverage.xml
gocov test bitbucket.org/jfriedland/gocovsta/pkg/... -v | gocov-xml > output/coverage.xml

# Convert output/coverage.xml to Bitbucket Server JSON payload
gocovsta output/coverage.xml > output/coverage.json
```

Then POST to your Bitbucket Server instance:

```shell
curl -X POST -H "Content-Type: application/json" --data @output/coverage.json -u ${USERNAME}:${PASSWORD} \
    "https://${SERVER_ADDRESS}/rest/code-coverage/1.0/commits/$(shell git rev-parse HEAD 2>/dev/null)" 1>/dev/null
```
