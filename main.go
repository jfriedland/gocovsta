/*
Package main implements the CLI for transforming Cobertura XML coverage reports
into JSON output, for consumption by the Bitbucket Server code coverage plugin
API.
*/
package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/jfriedland/gocovsta/pkg/gocovsta"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalln("usage: ./gocovsta <path/to/coverage.xml>")
	}
	xmlFile, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatalf("error opening input file: %+v\n", err)
	}
	defer xmlFile.Close()

	coverage, err := gocovsta.ParseInput(xmlFile)
	if err != nil {
		log.Fatalf("error parsing XML input file: %+v\n", err)
	}

	output := gocovsta.ParseCoverage(coverage)
	j, err := output.JSON()
	if err != nil {
		log.Fatalf("error parsing coverage to JSON: %+v\n", err)
	}
	fmt.Println(j)
}
