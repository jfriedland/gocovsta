all: gocovsta

clean:
	rm -f gocovsta output/coverage.xml output/coverage.json

gocovsta: build

output:
	mkdir -p output/

build:
	go build -o gocovsta .

ci-lint:
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v1.43.0
	./bin/golangci-lint run

test: output
	gocov test bitbucket.org/jfriedland/gocovsta/pkg/... -v | gocov-xml > output/coverage.xml

demo: deps test build json post

json: output build
	./gocovsta output/coverage.xml > output/coverage.json

deps:
	go get -u -v github.com/axw/gocov/...
	go get -u -v github.com/AlekSi/gocov-xml

post:
	@echo "Posting to: https://stash.atlassian.com/rest/code-coverage/1.0/commits/$(shell git rev-parse HEAD 2>/dev/null)"
	@curl -X POST -H "Content-Type: application/json" --data @output/coverage.json -u $${STASH_USERNAME}:$${STASH_PASSWORD} \
		"https://stash.atlassian.com/rest/code-coverage/1.0/commits/$(shell git rev-parse HEAD 2>/dev/null)" 1>/dev/null

.PHONY: all clean build ci-lint test demo deps post
